Component({
  props: {
    id: '',
    title: '',
    imgSrc: '',
    type: '',
    desc: '',
    onTap: () => {}
  },
  methods: {
    onTapCard(event) {
      this.props.onTap(event.target.dataset.id)
    }
  },
})