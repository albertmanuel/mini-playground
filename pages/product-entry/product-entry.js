const app = getApp()

Page({
  data: {},
  onLoad(query) {
    // search product from globalData using productId
    const productId = parseInt((new URLSearchParams(query)).get('id'))
    const product = app.globalData.jolibee.find(({id}) => id === productId)

    this.setData({ ...product })
  }
})