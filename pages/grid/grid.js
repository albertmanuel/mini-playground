const app = getApp()

Page({
  data: {
    items: app.globalData.users.map(u => ({
      icon: u.avatar,
      text: `${u.first_name} ${u.last_name}`,
      desc: u.email,
    }))
  },
})