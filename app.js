import {
  MOCK_USERS,
  MOCK_JOLIBEE,
} from '/api/mock'

App({
  globalData: {
    users: MOCK_USERS,
    jolibee: MOCK_JOLIBEE,
  },
  onLaunch(options) {
    // Page opens for the first time
    console.info('App launch', options);
  },
  onShow(options) {
    // Reopened by scheme from the background
    console.info('App foreground', options)
  },
  onError(error) {
    console.error('App error', error)
  },
});
